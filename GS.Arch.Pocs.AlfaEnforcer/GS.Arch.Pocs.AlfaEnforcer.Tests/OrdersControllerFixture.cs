using Xunit;
using FluentAssertions;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Controllers;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Models;
using Moq;
using Rsk.Enforcer.PEP;
using Rsk.Enforcer.PolicyModels;
using Rsk.Enforcer.PIP;

namespace GS.Arch.Pocs.AlfaEnforcer.Tests
{
    public class OrdersControllerFixture
    {
        [Fact]
        public async Task WhenGetAllParticipants_ThenAAllAreReturned()
        {
            Mock<IPolicyEnforcementPoint> pepMock = new Mock<IPolicyEnforcementPoint>();
            PolicyEvaluationOutcome outcome = new PolicyEvaluationOutcome(Rsk.Enforcer.PolicyModels.PolicyOutcome.Permit, new List<PolicyEvaluationAction>());
            pepMock.Setup(_ => _.Evaluate(It.IsAny<IAttributeValueProvider>())).ReturnsAsync(outcome);
            var sut = new OrdersController(new OrdersService(), pepMock.Object);
            var result = (OkObjectResult)await sut.GetAll(131);

            var orders = (IEnumerable<Order>)result.Value;
            orders.Should().HaveCount(2);
        }
    }
}
