using System.Collections.Generic;
using System.Threading.Tasks;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Models;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public interface IOrdersService
    {
        Task<IEnumerable<Order>> GetAllAsync(int clientId);

        Task<Order> GetOrder(int clientId, int orderId);
    }
}