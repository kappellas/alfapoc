using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GS.Arch.Pocs.AlfaEnforcer.Api.Models.Enums;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        List<UserAuthorizationDto> users = new List<UserAuthorizationDto>{
            new UserAuthorizationDto
            {
                Username = "markpurcell",
                Authorization = new UserAuth
                {
                    Groups = new List<UserGroup>
                    {
                        new UserGroup
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 1,
                                TenantType = TenantType.Reseller,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.GlobalAdmin),
                            Rules = new List<PolicyRules>
                            {
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.ParticipantId),
                                    Values = new List<string> { "All" }
                                }
                            },
                            Tenants = new List<Tenant>
                            {
                                new Tenant
                                {
                                    Id = 2,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 1
                                },
                                new Tenant
                                {
                                    Id = 3,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 2
                                },
                                new Tenant
                                {
                                    Id = 4,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 3
                                },
                                new Tenant
                                {
                                    Id = 5,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 4
                                }
                            }
                        },
                        new UserGroup
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.CompanyAdmin),
                            Tenants = new List<Tenant>
                            {
                                new Tenant
                                {
                                    Id = 2,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 1
                                }
                            }
                        }
                    },
                    UserRoles = new List<UserRole>
                    {
                        new UserRole
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.Participant),
                            ParticipantId = 123
                        }
                    }
                }
            },
            new UserAuthorizationDto
            {
                Username = "jamesmcenery",
                Authorization = new UserAuth
                {
                    Groups = new List<UserGroup>
                    {
                        new UserGroup
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.CompanyAdmin),
                            Rules = new List<PolicyRules>
                            {
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.Country),
                                    Values = new List<string> { "Ireland" }
                                },
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.BusinessUnit),
                                    Values = new List<string> { "Cork" }
                                }
                            },
                            Tenants = new List<Tenant>
                            {
                                new Tenant
                                {
                                    Id = 2,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 1
                                }
                            }
                        }
                    },
                    UserRoles = new List<UserRole>
                    {
                        new UserRole
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.Participant),
                            ParticipantId = 1231
                        }
                    }
                }
            },
            new UserAuthorizationDto
            {
                Username = "kappellas",
                Authorization = new UserAuth
                {
                    UserRoles = new List<UserRole>
                    {
                        new UserRole
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.Participant),
                            ParticipantId = 1234
                        }
                    }                  
                }
            },
            new UserAuthorizationDto
            {
                Username = "sdamico",
                Authorization = new UserAuth
                {
                    Groups = new List<UserGroup>
                    {
                        new UserGroup
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.FinancialAdvisorReadOnly),
                            Rules = new List<PolicyRules>
                            {
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.Country),
                                    Values = new List<string> { "Ireland" }
                                },
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.BusinessUnit),
                                    Values = new List<string> { "Cork" }
                                }
                            },
                            Tenants = new List<Tenant>
                            {
                                new Tenant
                                {
                                    Id = 2,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 1
                                }
                            }
                        }
                    }
                }
            },
            new UserAuthorizationDto
            {
                Username = "pino",
                Authorization = new UserAuth
                {
                    Groups = new List<UserGroup>
                    {
                        new UserGroup
                        {
                            Id = 1,
                            Tenant = new Tenant
                            {
                                Id = 2,
                                TenantType = TenantType.Client,
                                TenantTypeId = 1
                            },
                            Role = nameof(RoleType.FinancialAdvisorWrite),
                            Rules = new List<PolicyRules>
                            {
                                new PolicyRules
                                {
                                    TagName = nameof(TagName.Country),
                                    Values = new List<string> { "Spain" }
                                }
                            },
                            Tenants = new List<Tenant>
                            {
                                new Tenant
                                {
                                    Id = 2,
                                    TenantType = TenantType.Client,
                                    TenantTypeId = 1
                                }
                            }
                        }
                    }
                }
            }
        };

        public Task<UserAuthorizationDto> GetUserAuthorization(string username)
        {
            return Task.FromResult(this.users.FirstOrDefault(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase)));
        }

        public Task<UserAuthorizationDto> GetUserClientAuthorization(string username, int clientId)
        {
            UserAuthorizationDto user = this.users.FirstOrDefault(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (user == null)
                return null;

            UserAuthorizationDto clientUser = new UserAuthorizationDto
            {
                Username = user.Username,
                Authorization = new UserAuth
                {
                    Groups = new List<UserGroup>(),
                    UserRoles = new List<UserRole>()
                }
            };

            List<UserGroup> groups = new List<UserGroup>();
            List<UserRole> userRoles = new List<UserRole>();
            if (user.Authorization?.Groups != null)
            {
                foreach (var group in user.Authorization?.Groups?.Where(g => g.Tenants.Any(t => t.TenantType == TenantType.Client && t.TenantTypeId == clientId)))
                {
                    groups.Add(new UserGroup
                    {
                        Id = group.Id,
                        Role = group.Role,
                        Rules = group.Rules,
                        Tenant = group.Tenant,
                        Tenants = group.Tenants.Where(t => t.TenantType == TenantType.Client && t.TenantTypeId == clientId)
                    });
                }
            }
            if (user.Authorization?.UserRoles != null)
            {
                foreach (var userRole in user.Authorization?.UserRoles?.Where(t => t.Tenant.TenantType == TenantType.Client && t.Tenant.TenantTypeId == clientId))
                {
                    userRoles.Add(userRole);
                }
            }

            clientUser.Authorization.Groups = groups;
            clientUser.Authorization.UserRoles = userRoles;

            if (!clientUser.Authorization.Groups.Any() && !clientUser.Authorization.UserRoles.Any())
                return null;

            return Task.FromResult(clientUser);
        }
    }
}