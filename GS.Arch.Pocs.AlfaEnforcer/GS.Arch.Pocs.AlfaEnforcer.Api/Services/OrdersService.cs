using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Models;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class OrdersService : IOrdersService
    {
        List<Order> orders = new List<Order>{ 
            new Order
            {
                Id = 1,
                ParticipantId = 1234,
                ClientId = 1,
                Currency = "EUR",
                Quantity = 100,
                Country = "Spain",
                    
            },
            new Order
            {
                Id = 2,
                ParticipantId = 1234,
                ClientId = 1,
                Currency = "EUR",
                Quantity = 50,
                Country = "Ireland"
            }};

        public async Task<IEnumerable<Order>> GetAllAsync(int clientId)
        {
            var randomGen = new Random();

            for (int i = 3; i < 50; i++)
            {
                orders.Add(new Order
                {
                    Id = i,
                    ParticipantId = i,
                    ClientId = 1,
                    Quantity = randomGen.Next(100),
                    BusinessUnit = i % 3 == 0 ? "Cork" : "Lisbon",
                    Country = i % 3 == 0 ? "Ireland" : "Portugal"
                });
            }

            return await Task.FromResult(orders.Where(o => o.ClientId == clientId));
        }

        public async Task<Order> GetOrder(int clientId, int orderId)
        {
            return await Task.FromResult((await GetAllAsync(clientId)).FirstOrDefault(o => o.ClientId == clientId && o.Id == orderId));
        }
    }
}