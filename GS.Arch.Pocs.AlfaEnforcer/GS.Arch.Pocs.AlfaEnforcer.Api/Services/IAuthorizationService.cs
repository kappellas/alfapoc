using System.Threading.Tasks;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public interface IAuthorizationService
    {
        Task<UserAuthorizationDto> GetUserAuthorization(string username);

        Task<UserAuthorizationDto> GetUserClientAuthorization(string username, int clientId);
    }
}