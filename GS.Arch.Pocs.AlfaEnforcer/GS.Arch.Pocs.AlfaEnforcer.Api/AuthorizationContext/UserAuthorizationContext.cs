﻿using Rsk.Enforcer.PDP;
using Rsk.Enforcer.PIP;
using Rsk.Enforcer.PolicyModels;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Controllers
{
    public class UserAuthorizationContext : AuthorizationContext<UserAuthorizationContext>
    {
        public UserAuthorizationContext(string resourceType, string action) : base(resourceType, action)
        {
        }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "clientTenant")]
        public string Client { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "recordClient")]
        public string? RecordClient { get; set; }
        
        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "filterCountryValue")]
        public string Country { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "filterBusinessUnitValue")]
        public string BusinessUnit { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "filterParticipantIdValue")]
        public int? ParticipantId { get; set; }

    }
}