﻿using Rsk.Enforcer.PIP;
using Rsk.Enforcer.PolicyModels;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Providers.Models
{
    public class UserAuthorization
    {
        [PolicyAttributeValue(PolicyAttributeCategories.Subject, "role")]
        public string[] Role { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "userClient")]
        public int[] Clients { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Subject, "userParticipantId")]
        public int[] ParticipantId { get;  set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "UserBusinessUnitValue")]
        public string[] BusinessUnits { get; set; }

        [PolicyAttributeValue(PolicyAttributeCategories.Resource, "UserCountryValue")]
        public string[] Countries { get; set; }
    }
}