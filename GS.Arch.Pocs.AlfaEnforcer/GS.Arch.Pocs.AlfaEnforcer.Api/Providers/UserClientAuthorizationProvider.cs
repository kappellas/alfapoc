﻿using GS.Arch.Pocs.AlfaEnforcer.Api.Models.Enums;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Providers.Models;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Services;
using Rsk.Enforcer.PIP;
using Rsk.Enforcer.PolicyModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Providers
{
    internal class UserClientAuthorizationProvider : RecordAttributeValueProvider<UserAuthorization>
    {
        private static readonly PolicyAttribute Username =
           new PolicyAttribute("name", PolicyValueType.String, PolicyAttributeCategories.Subject);

        private static readonly PolicyAttribute ClientId =
          new PolicyAttribute("clientTenant", PolicyValueType.String, PolicyAttributeCategories.Resource);

        private readonly AuthorizationService authorizationService;

        public UserClientAuthorizationProvider()
        {
            this.authorizationService = new AuthorizationService();
        }

        protected override async Task<UserAuthorization> GetRecordValue(IAttributeResolver attributeResolver)
        {
            string username = (await attributeResolver.Resolve<string>(Username)).First();
            int clientId = int.Parse((await attributeResolver.Resolve<string>(ClientId)).First());

            UserAuthorizationDto user = await this.authorizationService.GetUserClientAuthorization(username, clientId);

            var userAuth = new UserAuthorization
            {
                Role = user.Authorization.Groups.Where(g => g.Rules != null && g.Rules.Any())
                                                .Select(g => g.Role)
                                                .Union(user.Authorization.UserRoles.Select(g => g.Role))
                                                .ToArray(),
                Clients = new int[] { clientId },
                ParticipantId = user.Authorization.UserRoles.Select(g => g.ParticipantId).ToArray(),
                BusinessUnits = user.Authorization.Groups.Where(g => g.Rules != null && g.Rules.Any())
                                                         .SelectMany(g => g.Rules.Where(t => t.TagName == nameof(TagName.BusinessUnit)))
                                                         .SelectMany(t => (IEnumerable<string>)t.Values)
                                                         .ToArray(),
                Countries = user.Authorization.Groups.Where(g => g.Rules != null && g.Rules.Any())
                                                     .SelectMany(g => g.Rules.Where(t => t.TagName == nameof(TagName.Country)))
                                                     .SelectMany(t => (IEnumerable<string>)t.Values)
                                                     .ToArray()
            };

            return userAuth;
        }
    }
}