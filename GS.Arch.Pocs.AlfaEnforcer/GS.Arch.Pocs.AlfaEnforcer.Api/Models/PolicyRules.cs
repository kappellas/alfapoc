﻿using System.Collections.Generic;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class PolicyRules
    {
        public string TagName { get; set; }

        public IEnumerable<object> Values { get; set; }
    }
}