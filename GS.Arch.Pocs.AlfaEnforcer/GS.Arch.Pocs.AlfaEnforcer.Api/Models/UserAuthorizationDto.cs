﻿namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class UserAuthorizationDto
    {
        public string Username { get; set; }

        public UserAuth Authorization { get; set; }
    }
}