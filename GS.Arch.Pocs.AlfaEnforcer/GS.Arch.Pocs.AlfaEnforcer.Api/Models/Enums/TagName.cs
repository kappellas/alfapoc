﻿namespace GS.Arch.Pocs.AlfaEnforcer.Api.Models.Enums
{
    public enum TagName
    {
        ParticipantId,
        BusinessUnit,
        Country
    }
}
