﻿namespace GS.Arch.Pocs.AlfaEnforcer.Api.Models.Enums
{
    public enum RoleType
    {
        GlobalAdmin,
        CompanyAdmin,
        Participant,
        FinancialAdvisorReadOnly,
        FinancialAdvisorWrite
    }
}
