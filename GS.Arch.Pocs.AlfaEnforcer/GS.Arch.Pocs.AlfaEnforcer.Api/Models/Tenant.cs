﻿namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class Tenant
    {
        public int Id { get; set; }

        public TenantType TenantType { get; set; }

        public int TenantTypeId { get; set; }
    }

    public enum TenantType
    {
        Client,

        Partner,

        Reseller
    }
}