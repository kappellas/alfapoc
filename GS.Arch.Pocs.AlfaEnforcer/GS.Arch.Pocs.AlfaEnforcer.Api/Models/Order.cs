namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ParticipantId { get; set; }
        public string Currency { get; set; }
        public int Quantity { get; set; }
        public string BusinessUnit { get; internal set; }
        public string Country { get; internal set; }
    }
}