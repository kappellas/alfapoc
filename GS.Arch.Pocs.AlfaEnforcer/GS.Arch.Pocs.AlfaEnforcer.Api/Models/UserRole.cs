﻿namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class UserRole
    {
        public int Id { get; set; }

        public Tenant Tenant { get; set; }

        public int ParticipantId { get; set; }

        public string Role { get; set; }
    }
}