﻿using System.Collections.Generic;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class UserAuth
    {
        public IEnumerable<UserGroup> Groups { get; set; }

        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}