﻿using System.Collections.Generic;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Services
{
    public class UserGroup
    {
        public int Id { get; set; }

        public Tenant Tenant { get; set; }

        public string Role { get; set; }

        public IEnumerable<Tenant> Tenants { get; set; }

        public IEnumerable<PolicyRules> Rules { get; set; }
    }
}