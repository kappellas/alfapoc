﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Models;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rsk.Enforcer.PEP;
using Rsk.Enforcer.PolicyModels;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Controllers
{
    [Authorize]
    [Route("Clients/{clientId}/[controller]")]
    [ApiController]
    
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersService ordersService;
        private readonly IPolicyEnforcementPoint pep;
        

        public OrdersController(
            IOrdersService ordersService, 
            IPolicyEnforcementPoint pep)
        {
            this.ordersService = ordersService;
            this.pep = pep;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int clientId)
        {
            UserAuthorizationContext provider = new UserAuthorizationContext("order", "getOrders")
            {
                Client = clientId.ToString()
            };
            
            var authorizationResult = await pep.Evaluate(provider);
            if (authorizationResult.Outcome != PolicyOutcome.Permit)
            {
                return Forbid();
            }

            var orders = await ordersService.GetAllAsync(clientId);
            var context = new UserAuthorizationContext("order", "getOrder")
            {
                Client = clientId.ToString()
            };

            var outputOrders = new List<Order>();
            foreach (var order in orders)
            {
                context.Country = order.Country;
                context.BusinessUnit = order.BusinessUnit;
                context.RecordClient = order.ClientId.ToString();

                var authResult = await pep.Evaluate(context);
                if (authResult.Outcome != PolicyOutcome.Permit)
                {
                    continue;
                }
                outputOrders.Add(order);
            }

            return Ok(outputOrders);
        }

        [HttpGet]
        [Route("{orderId}")]
        public async Task<IActionResult> Get(int clientId, int orderId)
        {
            Order order = await ordersService.GetOrder(clientId, orderId);

            var context = new UserAuthorizationContext("order", "getOrder")
            {
                Client = clientId.ToString(),
                Country = order.Country,
                BusinessUnit = order.BusinessUnit,
                RecordClient = order.ClientId.ToString()
            };

            var authResult = await pep.Evaluate(context);
            if (authResult.Outcome != PolicyOutcome.Permit)
            {
                return Forbid();
            }

            return Ok(order);
        }


        [HttpPut]
        [Route("{orderId}")]
        public async Task<IActionResult> Update(int clientId, int orderId)
        {
            Order order = await ordersService.GetOrder(clientId, orderId);

            var context = new UserAuthorizationContext("order", "editOrders")
            {
                Client = clientId.ToString(),
                Country = order.Country,
                BusinessUnit = order.BusinessUnit,
                RecordClient = order.ClientId.ToString(),
                ParticipantId = order.ParticipantId
            };

            var authResult = await pep.Evaluate(context);
            if (authResult.Outcome != PolicyOutcome.Permit)
            {
                return Forbid();
            }

            return Ok(order);
        }

    }
}
