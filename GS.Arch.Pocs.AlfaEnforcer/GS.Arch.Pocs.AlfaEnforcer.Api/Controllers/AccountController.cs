﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        /// <summary>
        /// Generate token
        /// </summary>
        /// <param name="username">username</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GenerateToken")]
        public async Task<string> PostGenerateToken(string username)
        {
            SecurityTokenDescriptor tokenDescriptor = GetTokenDescriptor(username);
            
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(securityToken);
        }

        private SecurityTokenDescriptor GetTokenDescriptor(string username)
        {
            byte[] securityKey = Encoding.UTF8.GetBytes("xP7+boNyrxRpZPpioPnhYVAtTcZwm8zzCAwvbp4DwA4=");
            var symmetricSecurityKey = new SymmetricSecurityKey(securityKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new List<Claim> { new Claim("name", username) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            return tokenDescriptor;
        }
    }

}
