using GS.Arch.Pocs.AlfaEnforcer.WebApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using Serilog;
using Rsk.Enforcer;
using GS.Arch.Pocs.AlfaEnforcer.Config;
using Rsk.Enforcer.PEP;
using Rsk.Enforcer.AspNetCore;
using GS.Arch.Pocs.AlfaEnforcer.WebApi.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace GS.Arch.Pocs.AlfaEnforcer.WebApi
{
    public class Startup
    {
        private const string PoliciesRootFolder = "Policies";
        private const string RootPolicy = "GS.Policies.Global";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));

            services.AddControllers();
            services.AddHttpContextAccessor();

            services.AddScoped<IOrdersService, OrdersService>();

            this.ConfigureAuth(services);

            services.AddEnforcer(RootPolicy, o =>
            {
                o.Licensee = EnforcerDemoLicense.Licensee;
                o.LicenseKey = EnforcerDemoLicense.LicenseKey;
            })
            .AddFileSystemPolicyStore(PoliciesRootFolder)
            .AddPolicyEnforcementPoint(o => o.Bias = PepBias.Deny)
            .AddDefaultAdviceHandling()
            .AddPolicyAttributeProvider<UserClientAuthorizationProvider>()
            .AddClaimsAttributeValueProvider(o => { });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GS.Arch.Pocs.AlfaEnforcer.WebApi", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"", Name = "Authorization", In = ParameterLocation.Header, Type = SecuritySchemeType.ApiKey, Scheme = "Bearer" });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("v1/swagger.json", "GS.Arch.Pocs.AlfaEnforcer.WebApi v1"));
            }
            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Configure Authentication
        /// </summary>
        protected virtual void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwtOptions =>
            {
                jwtOptions.SaveToken = true;
                jwtOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("xP7+boNyrxRpZPpioPnhYVAtTcZwm8zzCAwvbp4DwA4=")),
                    ValidateLifetime = true,
                    LifetimeValidator = LifetimeValidator
                };
            });
        }

        private static bool LifetimeValidator(
            DateTime? notBefore,
            DateTime? expires,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            return expires != null && expires > DateTime.Now;
        }
    }
}
